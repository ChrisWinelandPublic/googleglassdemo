package com.FISMobile.fisglass.Classes;

public enum CardViewType {
	BankingMainCardType,
	AccountsCardType,
	TransfersCardType,
	BillPayCardType
}
