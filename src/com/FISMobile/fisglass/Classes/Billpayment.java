package com.FISMobile.fisglass.Classes;

import java.util.Date;

public class Billpayment {
	
	String payee;
	String account;
	String amount;
	Date date;
	
	public String getPayee() {
		return payee;
	}

	public void setPayee(String payee) {
		this.payee = payee;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Billpayment(){
		super();
	}

	public boolean isLate(){
		return false;
	}
	
}
