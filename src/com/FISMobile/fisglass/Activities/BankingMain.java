package com.FISMobile.fisglass.Activities;

import java.util.ArrayList;
import java.util.List;

import com.FISMobile.fisglass.R;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;
import com.google.android.glass.app.Card;
import com.FISMobile.fisglass.Views.BankingMainCard;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

public class BankingMain extends Activity {

    private CardScrollView mCardScroller;
    private List<Card> mCards;
    
    /** Listener that displays the options menu when the card scroller is tapped. */ 
    private final AdapterView.OnItemClickListener mOnClickListener =
            new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        	if(position == 0){
        		//accounts
        		navigateTo(AccountsMain.class);
        	} else if(position == 1){
        		//transfers
        		navigateTo(TransfersMain.class);
        	} else if(position == 2){
        		//billpay
        		navigateTo(BillpayMain.class);
        	}
        }
    };
   
    private void navigateTo(Class className){
    	Intent intent = new Intent(this, className);
    	this.startActivity(intent);
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.default_base);
        
        createCards();
        
        mCardScroller = new CardScrollView(this);
        mCardScroller.setHorizontalScrollBarEnabled(true);
        BankingMainCardScrollAdapter adapter = new BankingMainCardScrollAdapter();
        mCardScroller.setAdapter(adapter);
        mCardScroller.setOnItemClickListener(mOnClickListener);
        mCardScroller.activate();
        setContentView(mCardScroller);
    }

	private void createCards() {
		mCards = new ArrayList<Card>();

        BankingMainCard card;

        card = new BankingMainCard(this);
        card.setCardTitle("Accounts");
        card.setCardImage(R.drawable.accounts);
        mCards.add(card);
        
        card = new BankingMainCard(this);
        card.setCardTitle("Transfers");
        card.setCardImage(R.drawable.transfers);
        mCards.add(card);
        
        card = new BankingMainCard(this);
        card.setCardTitle("Billpay");
        card.setCardImage(R.drawable.billpay);
        mCards.add(card);
	}
	
	public class BankingMainCardScrollAdapter extends CardScrollAdapter {

		@Override
        public int getPosition(Object item) {
            return mCards.indexOf(item);
        }

        @Override
        public int getCount() {
            return mCards.size();
        }

        @Override
        public Object getItem(int position) {
            return mCards.get(position);
        }

        @Override
        public int getViewTypeCount() {
            return Card.getViewTypeCount();
        }

        @Override
        public int getItemViewType(int position){
            return mCards.get(position).getItemViewType();
        }

        @Override
        public View getView(int position, View convertView,
                ViewGroup parent) {
            return  mCards.get(position).getView(convertView, parent);
        }
    }
	
}
