package com.FISMobile.fisglass.Activities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.FISMobile.fisglass.R;
import com.FISMobile.fisglass.Views.TransfersMainCard;
import com.google.android.glass.app.Card;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

public class TransfersMain extends Activity {
	private CardScrollView mCardScroller;
    private List<Card> mCards;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.default_base);
        
        createCards();
        
        mCardScroller = new CardScrollView(this);
        mCardScroller.setHorizontalScrollBarEnabled(true);
        TransfersMainCardScrollAdapter adapter = new TransfersMainCardScrollAdapter();
        mCardScroller.setAdapter(adapter);
        mCardScroller.activate();
        mCardScroller.setSelection(1);
        setContentView(mCardScroller);
        
    }
    
    private Date decrementDate(Date date){
    	Calendar c = Calendar.getInstance(); 
		c.setTime(date); 
		c.add(Calendar.DATE, -1);
		return c.getTime();
    }

	private void createCards() {
		mCards = new ArrayList<Card>();
		Date curDate = new Date();
		
		Calendar c = Calendar.getInstance(); 
		c.setTime(curDate); 
		c.add(Calendar.DATE, 1);
		curDate = c.getTime();
		
		TransfersMainCard card;
		
		card = new TransfersMainCard(this);
		card.setTransfer("$67.00", "Dad", "Checking-7356", curDate);
		mCards.add(card);
		curDate = decrementDate(curDate);
		
		card = new TransfersMainCard(this);
		card.setTransfer("$300.00", "Savings-2987", "Checking-7356", curDate);
		mCards.add(card);
		curDate = decrementDate(curDate);
		
		card = new TransfersMainCard(this);
		card.setTransfer("$4,381.65", "Mr. James", "Line Credit-9638", curDate);
		mCards.add(card);
		curDate = decrementDate(curDate);
		
		card = new TransfersMainCard(this);
		card.setTransfer("$20.00", "The Neighbors", "Line Credit-9638", curDate);
		mCards.add(card);
	}
	
	public class TransfersMainCardScrollAdapter extends CardScrollAdapter {

		@Override
        public int getPosition(Object item) {
            return mCards.indexOf(item);
        }

        @Override
        public int getCount() {
            return mCards.size();
        }

        @Override
        public Object getItem(int position) {
            return mCards.get(position);
        }

        @Override
        public int getViewTypeCount() {
            return Card.getViewTypeCount();
        }

        @Override
        public int getItemViewType(int position){
            return mCards.get(position).getItemViewType();
        }

        @Override
        public View getView(int position, View convertView,
                ViewGroup parent) {
            return  mCards.get(position).getView(convertView, parent);
        }
    }
}
