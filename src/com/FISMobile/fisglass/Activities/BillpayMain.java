package com.FISMobile.fisglass.Activities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.FISMobile.fisglass.R;
import com.FISMobile.fisglass.Views.BillpayMainCard;
import com.google.android.glass.app.Card;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

public class BillpayMain extends Activity {
	private CardScrollView mCardScroller;
    private List<Card> mCards;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.default_base);
        
        createCards();
        
        mCardScroller = new CardScrollView(this);
        mCardScroller.setHorizontalScrollBarEnabled(true);
        BillpayMainCardScrollAdapter adapter = new BillpayMainCardScrollAdapter();
        mCardScroller.setAdapter(adapter);
        mCardScroller.activate();
        mCardScroller.setSelection(3);
        setContentView(mCardScroller);
    }
    
    private Date decrementDate(Date date){
    	Calendar c = Calendar.getInstance(); 
		c.setTime(date); 
		c.add(Calendar.DATE, -1);
		return c.getTime();
    }

	private void createCards() {
		mCards = new ArrayList<Card>();
		
		BillpayMainCard card;
		Date curDate = new Date();
		
		Calendar c = Calendar.getInstance(); 
		c.setTime(curDate); 
		c.add(Calendar.DATE, 3);
		curDate = c.getTime();
		
		card = new BillpayMainCard(this);
		card.setBillpayment("Checking-7356", "$89.76", curDate, "AT&T");
		mCards.add(card);
		curDate = decrementDate(curDate);
		
		card = new BillpayMainCard(this);
		card.setBillpayment("Checking-7356", "$215.16", curDate, "Comcast");
		mCards.add(card);
		curDate = decrementDate(curDate);
		
		card = new BillpayMainCard(this);
		card.setBillpayment("Line Credit-9638", "$7.01", curDate, "Amazon");
		mCards.add(card);
		curDate = decrementDate(curDate);
		
		card = new BillpayMainCard(this);
		card.setBillpayment("Line Credit-9638", "$94.04", curDate, "PG&E");
		mCards.add(card);
		curDate = decrementDate(curDate);
		
		card = new BillpayMainCard(this);
		card.setBillpayment("Line Credit-9638", "$275.48", curDate, "Mazda Dealer");
		mCards.add(card);
		curDate = decrementDate(curDate);
		
		card = new BillpayMainCard(this);
		card.setBillpayment("Checking-7356", "$11.82", curDate, "The Man");
		mCards.add(card);
		curDate = decrementDate(curDate);
		
		card = new BillpayMainCard(this);
		card.setBillpayment("Checking-7356", "$45.00", curDate, "CapitalOne");
		mCards.add(card);
		curDate = decrementDate(curDate);
		
		card = new BillpayMainCard(this);
		card.setBillpayment("Savings-2987", "$75.00", curDate, "American Express");
		mCards.add(card);
		curDate = decrementDate(curDate);
		
		card = new BillpayMainCard(this);
		card.setBillpayment("Savings-2987", "$2,785.72", curDate, "Banana Republic");
		mCards.add(card);
		curDate = decrementDate(curDate);
		
		card = new BillpayMainCard(this);
		card.setBillpayment("Rainy Day-3009", "$0.76", curDate, "Gum Co.");
		mCards.add(card);
	}
	
	public class BillpayMainCardScrollAdapter extends CardScrollAdapter {

		@Override
        public int getPosition(Object item) {
            return mCards.indexOf(item);
        }

        @Override
        public int getCount() {
            return mCards.size();
        }

        @Override
        public Object getItem(int position) {
            return mCards.get(position);
        }

        @Override
        public int getViewTypeCount() {
            return Card.getViewTypeCount();
        }

        @Override
        public int getItemViewType(int position){
            return mCards.get(position).getItemViewType();
        }

        @Override
        public View getView(int position, View convertView,
                ViewGroup parent) {
            return  mCards.get(position).getView(convertView, parent);
        }
    }
}
