package com.FISMobile.fisglass.Activities;

import java.util.ArrayList;
import java.util.List;

import com.FISMobile.fisglass.R;
import com.FISMobile.fisglass.Views.AccountsMainCard;
import com.google.android.glass.app.Card;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

public class AccountsMain extends Activity {
	
    private CardScrollView mCardScroller;
    private List<Card> mCards;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.default_base);
        
        createCards();
        
        mCardScroller = new CardScrollView(this);
        mCardScroller.setHorizontalScrollBarEnabled(true);
        AccountsMainCardScrollAdapter adapter = new AccountsMainCardScrollAdapter();
        mCardScroller.setAdapter(adapter);
        mCardScroller.activate();
        setContentView(mCardScroller);
        
    }

	private void createCards() {
		mCards = new ArrayList<Card>();
		
		AccountsMainCard card;
		
		card = new AccountsMainCard(this);
		card.setAccount("Savings", "********2987", "$8,399.07");
		mCards.add(card);
		
		card = new AccountsMainCard(this);
		card.setAccount("Checking", "********7356", "$6,285.69");
		mCards.add(card);
		
		card = new AccountsMainCard(this);
		card.setAccount("Rainy Day", "********3009", "$157.54");
		mCards.add(card);
		
		card = new AccountsMainCard(this);
		card.setAccount("Home Loan", "********5629", "$236,584.24");
		mCards.add(card);
		
		card = new AccountsMainCard(this);
		card.setAccount("Market Savings", "********0001", "$1,490.46");
		mCards.add(card);
		
		card = new AccountsMainCard(this);
		card.setAccount("Line Credit", "********9638", "$3,736.67");
		mCards.add(card);
	}
	
	public class AccountsMainCardScrollAdapter extends CardScrollAdapter {

		@Override
        public int getPosition(Object item) {
            return mCards.indexOf(item);
        }

        @Override
        public int getCount() {
            return mCards.size();
        }

        @Override
        public Object getItem(int position) {
            return mCards.get(position);
        }

        @Override
        public int getViewTypeCount() {
            return Card.getViewTypeCount();
        }

        @Override
        public int getItemViewType(int position){
            return mCards.get(position).getItemViewType();
        }

        @Override
        public View getView(int position, View convertView,
                ViewGroup parent) {
            return  mCards.get(position).getView(convertView, parent);
        }
    }
}
