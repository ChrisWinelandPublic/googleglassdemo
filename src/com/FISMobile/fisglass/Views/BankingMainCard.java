package com.FISMobile.fisglass.Views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.FISMobile.fisglass.R;
import com.FISMobile.fisglass.Classes.CardViewType;

import com.google.android.glass.app.Card;

public class BankingMainCard extends Card{
	
	private LayoutInflater inflater;
	private String cardTitle;
	private int cardImageID;
	
	public BankingMainCard(Context context) {
		super(context);
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	}
	
	public int getItemViewType(){
		return CardViewType.BankingMainCardType.ordinal();
	}
	
	@Override
	public View getView(View convertView, ViewGroup parent){
		if (convertView == null){
			convertView = inflater.inflate(R.layout.banking_main_card, null);
		}
		populateElements(convertView);
		return convertView;
	}
	
	private void populateElements(View convertView){
		((TextView)convertView.findViewById(R.id.cardTitle)).setText(cardTitle);
		((ImageView)convertView.findViewById(R.id.cardImage)).setImageResource(cardImageID);
	}
	
	public void setCardTitle(String newCardTitle){
		cardTitle = newCardTitle;
	}
	
	public void setCardImage(int newCardImageID){
		cardImageID = newCardImageID;
	}
}
