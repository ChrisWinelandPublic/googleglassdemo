package com.FISMobile.fisglass.Views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.glass.app.Card;

import com.FISMobile.fisglass.R;
import com.FISMobile.fisglass.Classes.CardViewType;
import com.FISMobile.fisglass.Classes.Account;

public class AccountsMainCard extends Card {
	
	private LayoutInflater inflater;
	private Account account;
	
	public AccountsMainCard(Context context) {
		super(context);
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public int getItemViewType(){
		return CardViewType.AccountsCardType.ordinal();
	}
	
	@Override
	public View getView(View convertView, ViewGroup parent){
		if (convertView == null){
			convertView = inflater.inflate(R.layout.account_main_card, null);
		}
		populateElements(convertView);
		return convertView;
	}
	
	private void populateElements(View convertView){
		if(account != null){
			((TextView)convertView.findViewById(R.id.accountName)).setText(account.getAccountName());
			((TextView)convertView.findViewById(R.id.accountNumber)).setText(account.getAccountNumber());
			((TextView)convertView.findViewById(R.id.accountBalance)).setText(account.getAccountBalance());
		}
	}

	public void setAccount(String name, String id, String balance){
		if(account == null){
			account = new Account();
		}
		account.setAccountName(name);
		account.setAccountNumber(id);
		account.setAccountBalance(balance);
	}
}
