package com.FISMobile.fisglass.Views;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.FISMobile.fisglass.R;
import com.FISMobile.fisglass.Classes.Billpayment;
import com.google.android.glass.app.Card;

public class BillpayMainCard extends Card {
	
	private LayoutInflater inflater;
	private Billpayment billpayment;

	public BillpayMainCard(Context context) {
		super(context);
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(View convertView, ViewGroup parent){
		if (convertView == null){
			convertView = inflater.inflate(R.layout.billpay_main_card, null);
		}
		populateElements(convertView);
		return convertView;
	}
	
	private void populateElements(View convertView){
		if(billpayment != null){
			SimpleDateFormat format = 
		            new SimpleDateFormat("EEE, MMM d");
			
			((TextView)convertView.findViewById(R.id.payee)).setText(billpayment.getPayee());
			((TextView)convertView.findViewById(R.id.account)).setText(billpayment.getAccount());
			((TextView)convertView.findViewById(R.id.amount)).setText(billpayment.getAmount());
			((TextView)convertView.findViewById(R.id.date)).setText(format.format(billpayment.getDate()));
		}
	}
	
	public void setBillpayment(String account, String amount, Date date, String payee){
		if(billpayment == null){
			billpayment = new Billpayment();
		}
		billpayment.setAccount(account);
		billpayment.setAmount(amount);
		billpayment.setDate(date);
		billpayment.setPayee(payee);
	}
}
