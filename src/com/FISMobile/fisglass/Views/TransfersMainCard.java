package com.FISMobile.fisglass.Views;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.FISMobile.fisglass.R;
import com.FISMobile.fisglass.Classes.CardViewType;
import com.FISMobile.fisglass.Classes.Transfer;
import com.google.android.glass.app.Card;

public class TransfersMainCard extends Card {
	
	private LayoutInflater inflater;
	private Transfer transfer;

	public TransfersMainCard(Context context) {
		super(context);
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public int getItemViewType(){
		return CardViewType.TransfersCardType.ordinal();
	}
	
	@Override
	public View getView(View convertView, ViewGroup parent){
		if (convertView == null){
			convertView = inflater.inflate(R.layout.trainsfers_main_card, null);
		}
		populateElements(convertView);
		return convertView;
	}
	
	private void populateElements(View convertView){
		if(transfer != null){
			SimpleDateFormat format = 
		            new SimpleDateFormat("EEE, MMM d");
			((TextView)convertView.findViewById(R.id.transfer_amount)).setText(transfer.getAmount());
			((TextView)convertView.findViewById(R.id.transfer_destination)).setText(transfer.getDestination());
			((TextView)convertView.findViewById(R.id.transfer_source)).setText(transfer.getSource());
			((TextView)convertView.findViewById(R.id.transfers_date)).setText(format.format(transfer.getDate()));
		}
	}

	public void setTransfer(String amount, String destination, String source, Date date){
		if(transfer == null){
			transfer = new Transfer();
		}
		transfer.setAmount(amount);
		transfer.setDate(date);
		transfer.setDestination(destination);
		transfer.setSource(source);
	}
}
